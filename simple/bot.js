const Discord = require('discord.io');
const winston = require('winston');
const auth = require('./auth.json');

const logger = winston.createLogger({
    level: 'info',
    format: winston.format.simple(),
    transports: [
        // colorize the output to the console
        new winston.transports.Console({ colorize: true })
    ]
});
logger.level = "debug";

// Initialize Discord Bot
let bot = new Discord.Client({
    token: auth.token,
    autorun: true
});
bot.on('ready', function (evt) {
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(bot.username + ' - (' + bot.id + ')');
});
bot.on('message', function (user, userID, channelID, message, evt) {
    if (user !== bot.username && userID !== bot.id) {
        // Our bot will match a regexp to detect sentences ended with "quoi"
        let reOuais = /^.*(([Qq][uU]+[oO]+[iI]+)|([kK][oO]+([iI]+|[aA]+))|([kK][wW][aA]+))(\s*\.*\?*!*)*$/;
        const messagesOuais = ["Ouais!", "Ouuaaaaaaaaaaaaaaaaaaaaaaaaaaaaais !!", "Ouéééééééé!", "Mais Ouais !",
            "Non mais Ouais !"];

        let reRock = /([sS]axon)|([pP]raying\s?[mM]antis)|([Jj]udas\s?[pP]riest)|([aA][oO][rR])/;
        let reSaxon = /[sS]axon/;

        let reEdwin = /^\s*([Oo][kK])|([tT]'inqui[eè]te)|([tT]kt)|([dD]ac)|([dD]'accord)(\s*\.*!*\?*)*/;

        let reSteven = /.*([sS]teven)\s*([wW]ilson).*/;
        if (reOuais.test(message)) {

            bot.sendMessage({
                to: channelID,
                message: messagesOuais[Math.floor(Math.random() * messagesOuais.length)]
            });
        }
        else if (reSteven.test(message))
        {
            bot.sendMessage({
                to: channelID,
                message: "Elle est où Ninet ?"
            });
        }
        else if (reEdwin.test(message))
        {
            bot.sendMessage({
                to: channelID,
                message: "Azzzzzzzzzzzy Heeein !"
            });
        }
        else {
            if (reSaxon.test(message)) {
                bot.sendMessage({
                    to: channelID,
                    message: "<@260495684829052928> Tu connais pas Saxon ?"
                });
            }
            if (reRock.test(message)) {
                bot.sendMessage({
                    to: channelID,
                    message: "Ca c'est rock :metal: !"
                });
            }
        }

    }
});